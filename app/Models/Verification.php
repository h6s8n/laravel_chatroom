<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;




class Verification extends Model
{

	use SoftDeletes;
    
	protected $fillable =[
		'name',
		'family',
		'national_card',
		'phone_number',
		'mobile_number',
		'birthday_date',
		'address',
		'card_number',
		'sheba',
		'national_card_image_address',

	];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($file) {
            $file->identifier = uniqid(true);
        });
    }

    // public function scopeFinished(Builder $builder)
    // {
    //     return $builder->where('finished', true);
    // }
    
	protected $casts = [
        'finished' => 'datetime',
        'started' => 'datetime',
        'approved' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeFinished(Builder $builder)
    {
        return $builder->whereNotNull('finished');
    }

    public function getRouteKeyName()
    {
        return 'identifier';
    }

}
