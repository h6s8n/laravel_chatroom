<?php

namespace App\Models\Chat;
use DateTimeInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'body'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    protected $appends = [
        'selfOwned'
    ];

    public function getSelfOwnedAttribute()
    {
        return $this->user_id === auth()->user()->id;
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
} 
