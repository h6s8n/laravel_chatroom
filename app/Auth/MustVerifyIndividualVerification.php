<?php

namespace App\Auth;

use Illuminate\Auth\Notifications\VerifyEmail;


trait MustVerifyIndividualVerification
{
    /**
     * Determine if the user has verified their idndividual information.
     *
     * @return bool

     */
    public function hasVerifiedIndividualVerification()
    {
    	// dd(! is_null($this->individual_verified_at));
        return ! is_null($this->individual_verified_at);
    } 


/**
 * Mark the given user's idndividual as verified.
 * @return bool
 */
     function markIdndivIdualAsVerified() 
    {
       
        return $this->forceFill([
            'individual_verified_at' => $this->freshTimestamp(),
        ])->save();
    }




    public function hasFinishedForm()
    {

    return (bool) auth()->user()->Verification()->finished()->count();
    
    }


}