<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Foundation\Auth\VerifiesIndividualVerification;
use Illuminate\Support\Facades\Validator;
use App\Models\Verification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;


class IndividualVerificationController extends Controller
{

	use VerifiesIndividualVerification,HasTimestamps;


	protected $redirectTo = RouteServiceProvider::HOME;


	    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('signed')->only('showForm', 'sendForm');
        $this->middleware('verified');

        // $this->middleware('individual')->except('show');
        // $this->middleware('individual')->only('showForm');

        // $this->middleware('throttle:6,1')->only('showForm', 'sendForm');
        
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'family' => ['required', 'string', 'max:255'],
            'national_card' => ['required', 'string','numeric','min:10','unique:verifications'],
            'phone_number' => ['required', 'string','numeric'],
            'mobile_number' => ['required', 'string','numeric','min:10','unique:verifications'],
            'birthday_date' => ['required', 'string', 'before:today'],
            'address' => ['required', 'string', 'max:255'],
            'card_number' => ['required', 'string','numeric','min:16'],
            'sheba' => ['required', 'string','numeric'],
            // 'national_card_image_address' => ['required', 'image','mimes:jpeg,png,jpg,gif,svg','max:2048'],
            
            
        ]);
    }

    protected function create(array $data)
    {
        return auth()->user()->Verification()->create([
            'name' => $data['name'],
            'family' => $data['family'],
            'national_card' => $data['national_card'],
            'phone_number' => $data['phone_number'],
            'mobile_number' => $data['mobile_number'],
            'birthday_date' => $data['birthday_date'],
            'address' => $data['address'],
            'card_number' => $data['card_number'],
            'sheba' => $data['sheba'],
            // 'national_card_image_address' => $data['national_card_image_address'],
            'national_card_image_address'=>"img/addres/test",
           
        ]);
    }




    //  function forceKir(array $attributes)
    // {
    //     return static::unguarded(function () use ($attributes) {
    //         return $this->fill($attributes);
    //     });
    // }

    protected function markFinished()
    {
        return auth()->user()->Verification()->forceFill([
            'finished' => $this->freshTimestamp(),
        ])->save();
    }
    

    public function forceFill(array $attributes)
    {
        return static::unguarded(function () use ($attributes) {
            return $this->fill($attributes);
        });
    }

}
