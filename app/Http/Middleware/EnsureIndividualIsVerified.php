<?php

namespace App\Http\Middleware;

use Closure;
use App\Auth\MustVerifyIndividualVerification;
use Illuminate\Support\Facades\Redirect;

class EnsureIndividualIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $redirectToRoute
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        if (! $request->user() ||
            ! $request->user()->hasVerifiedIndividualVerification()) {
            return $request->expectsJson()
                    ? abort(403, 'Your individual information is not verified.')
                    : Redirect::route($redirectToRoute ?: 'security.individual');
        }

        return $next($request);
    }
}
