<?php

namespace App\Foundation\Auth;
use App\Models\Verification;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\RedirectsUsers;
use App\Http\Controllers\Controller;




trait VerifiesIndividualVerification
{
	use RedirectsUsers;

    public function show(Request $request,Verification $Verification)
    {
        $Verification = auth()->user()->Verification()->latest()->finished()->get();

        return $request->user()->hasVerifiedIndividualVerification()
            ? redirect($this->redirectPath())
            : view('auth.security.individual', [
            'Verification' => $Verification
            ]);

    	
    }


    public function showForm(Request $request)
    {
        $Verification = auth()->user()->Verification()->latest()->finished()->get();
      
            
        if ($request->user()->hasVerifiedIndividualVerification()) {

            return $request->wantsJson()
                ? new Response('', 204)
                : redirect($this->redirectPath());
        }
      
             return $request->user()->hasFinishedForm()
                ? redirect($this->redirectPath('security/individual'))
                : view('auth.security.verify', [
                'Verification' => $Verification
            ]);
        

    }


    public function sendForm(Request $request,Verification $Verification)
    {   

        if (! $this->checkVerificationExist()) {
            $this->validator($request->all())->validate();
        //     // add event later
            $user = $this->create($request->all());

            $Verification =Verification::where('user_id', $request->user()->id)->first();

            $Verification->finished = time();
            $Verification->save();
            return redirect($this->redirectPath('security/individual'))
            ->withSuccess('Thanks, submitted for review.');
        }



            return redirect($this->redirectPath('security/individual'))
            ->withSuccess('Thanks, submitted for review.');
        

         
       





    
    }
    protected function createAndReturnSkeletonFile()
    {
        return auth()->user()->Verification()->create([
            'name' => 'Untitled',
            'family'=> 'None',
            'national_card'=> 'None',
            'phone_number'=> 'None',
            'mobile_number'=> 'None',
            'birthday_date'=> 'None',
            'address'=> 'None',
            'address'=> 'None',
            'card_number'=> 'None',
            'sheba'=> 'None',
            // 'national_card_image_address'=> 'None',
            'finished' => false,
            'started' => false,
        ]);
    }

    protected function checkVerificationExist()
    {

        return auth()->user()->Verification()->exists();
    } 


}