<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Verification;
use Illuminate\Auth\Access\HandlesAuthorization;

class VerificationPolicy
{
    use HandlesAuthorization;

    public function touch(User $user, Verification $Verification)
    {
        // dd($Verification->user_id);
        return $user->id == $Verification->user_id;
    }
}
