@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5><b>{{ __('Individual Verification') }}</b></h5></div>

                <div class="card-body">
                    <form method="POST"  action="{{ route('security.verify') }}">
                        @csrf

                        <div class="form-group row">
							<div class="col-12">
								<div class="card">
									<div class="card-header"> 
										<b>personal information</b>
									</div>
								</div> 
							</div>
						</div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="family" class="col-md-4 col-form-label text-md-right">{{ __('family') }}</label>

                            <div class="col-md-4">
                                <input id="family" type="text" class="form-control @error('family') is-invalid @enderror" name="family" value="{{ old('family') }}" required autocomplete="family">

                                @error('family')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="national_card" class="col-md-4 col-form-label text-md-right">{{ __('national_card') }}</label>

                            <div class="col-md-4">
                                <input id="national_card" type="text" class="form-control @error('national_card') is-invalid @enderror" name="national_card" required autocomplete="new-national_card">

                                @error('national_card')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('phone_number') }}</label>

                            <div class="col-md-4">
                                <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number">

                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile_number" class="col-md-4 col-form-label text-md-right">{{ __('mobile_number') }}</label>

                            <div class="col-md-4">
                                <input id="mobile_number" type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" value="{{ old('mobile_number') }}" required autocomplete="mobile_number">

                                @error('mobile_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birthday_date" class="col-md-4 col-form-label text-md-right">{{ __('birthday_date') }}</label>

                            <div class="col-md-4">
                                <input id="birthday_date" type="text" class="form-control @error('birthday_date') is-invalid @enderror" name="birthday_date" value="{{ old('birthday_date') }}" required autocomplete="birthday_date">

                                @error('birthday_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                   		<div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('address') }}</label>

                        	<div class="col-md-4">
								<textarea id="address" name="address" class="form-control @error('address') is-invalid @enderror" value="{{ old('address') }}" required autocomplete="address" ></textarea>


 {{--                            
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address"> --}}

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
							<div class="col-12">
								<div class="card">
									<div class="card-header"> 
										<b>card information</b>
									</div>
								</div> 
							</div>
						</div>

                        <div class="form-group row">
                            <label for="card_number" class="col-md-4 col-form-label text-md-right">{{ __('card_number') }}</label>

                            <div class="col-md-4">
                                <input id="card_number" type="text" class="form-control @error('card_number') is-invalid @enderror" name="card_number" value="{{ old('card_number') }}" required autocomplete="card_number">

                                @error('card_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sheba" class="col-md-4 col-form-label text-md-right">{{ __('sheba') }}</label>

                            <div class="col-md-4">
                                <input id="sheba" type="text" class="form-control @error('sheba') is-invalid @enderror" name="sheba" value="{{ old('sheba') }}" required autocomplete="sheba">

                                @error('sheba')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
{{--                         <div class="form-group row">
                            <label for="national_card_image" class="col-md-4 col-form-label text-md-right">{{ __('national_card_image') }}</label>

                            <div class="col-md-4"> --}}
{{--                                 <input id="national_card_image_address" type="national_card_image_address" class="form-control @error('national_card_image_address') is-invalid @enderror" name="national_card_image_address" value="{{ old('national_card_image_address') }}" required autocomplete="national_card_image_address"> --}}

{{-- 							<input id="national_card_image" type="file"  class="form-control-file @error('national_card_image') is-invalid @enderror" class="hidden"  name="national_card_image_address" value="{{ old('national_card_image') }}" required autocomplete="national_card_image" >

                                @error('national_card_image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> --}}

{{--                         <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-4">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div> --}}

                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
