<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => [
                'authenticated' => auth()->check(),
                'id' => auth()->check() ? auth()->user()->id : null,
                'name' => auth()->check() ? auth()->user()->name : null
            ],

        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

<!--                         @if (Auth::user()->hasRole('admin'))
                            <li><a class="nav-link" href="">Admin panel</a></li>
                        @endif -->
                        @role('admin')
                            <li><a class="nav-link" href="">Admin panel</a></li>
                        @endrole


                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


        <main class="py-4">
            @yield('content')
        </main>


        {{-- crypto price --}}
{{--         <main class="py-4">
            <aside class="left-sidebar">
                <section class="markets">
                    <div class="col-md-12">
                        <div class="markets_first_wrapper">
                            <table class="table table-hover market_table">
                                <thead>
                                    <tr>
                                        <th width="40%" height="40px" style="border-radius: 0 20px 0 0;">نوع ارز</th>
                                        <th width="35%" height="40px">قیمت($)</th>
                                        <th width="25%" height="40px" style="border-radius: 20px 0 0 0;">تغییرات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="coin-type pr_increased pr_decreased" data-key="BTC" id="bitcoin" style="border: none;">
                                        <td width="40%" class="coin-name" style="border: none;">
                                            <a class="market_table_for_tablet" data-link="https://wallex.ir/app/trade/btc-tmn" href="https://wallex.ir/app/trade/btc-tmn">
                                                <img class="hidden-md" src="https://wallex.ir/img/coins/btc.png" alt="Bitcoin" width="20px" /> بیت کوین
                                            </a>
                                        </td>
                                        <td width="35%" class="price" id="WS-BTCUSDT" style="border: none;">9,314</td>
                                        <td width="25%" class="inc_change_pr" dir="ltr" style="border: none; color: #01dc8a;">1.63%</td>
                                    </tr>
                                    <tr class="coin-type" data-key="ETH" id="ethereum" style="border: none;">
                                        <td width="40%" class="coin-name" style="border: none;">
                                            <a class="market_table_for_tablet" data-link="https://wallex.ir/app/trade/eth-tmn" href="https://wallex.ir/app/trade/eth-tmn">
                                                <img class="hidden-md" src="https://wallex.ir/img/coins/eth.png" alt="Ethereum" width="20px" /> اتریوم
                                            </a>
                                        </td>
                                        <td width="35%" class="price" id="WS-ETHUSDT" style="border: none;">241</td>
                                        <td width="25%" class="inc_change_pr" dir="ltr" style="border: none; color: #01dc8a;">1.47%</td>
                                    </tr>
                                    <tr class="coin-type" data-key="BCH" id="bitcoin-cash" style="border: none;">
                                        <td width="40%" class="coin-name" style="border: none;">
                                            <a class="market_table_for_tablet" data-link="https://wallex.ir/app/trade/bch-tmn" href="https://wallex.ir/app/trade/bch-tmn">
                                                <img class="hidden-md" src="https://wallex.ir/img/coins/bch.png" alt="Bitcoin cash" width="20px" /> بیت کوین کش
                                            </a>
                                        </td>
                                        <td width="35%" class="price" id="WS-BCHUSDT" style="border: none;">229</td>
                                        <td width="25%" class="inc_change_pr" dir="ltr" style="border: none; color: #01dc8a;">1.62%</td>
                                    </tr>
                                    <tr class="coin-type" data-key="LTC" id="litecoin" style="border: none;">
                                        <td width="40%" class="coin-name" style="border: none;">
                                            <a class="market_table_for_tablet" data-link="https://wallex.ir/app/trade/ltc-tmn" href="https://wallex.ir/app/trade/ltc-tmn">
                                                <img class="hidden-md" src="https://wallex.ir/img/coins/ltc.png" alt="LiteCoin" width="20px" /> لایت کوین
                                            </a>
                                        </td>
                                        <td width="35%" class="price" id="WS-LTCUSDT" style="border: none;">43</td>
                                        <td width="25%" class="inc_change_pr" dir="ltr" style="border: none; color: #01dc8a;">1.96%</td>
                                    </tr>
                                    <tr class="coin-type" data-key="DASH" id="dash" style="border: none;">
                                        <td width="40%" class="coin-name" style="border: none;">
                                            <a class="market_table_for_tablet" data-link="https://wallex.ir/app/trade/dash-tmn" href="https://wallex.ir/app/trade/dash-tmn">
                                                <img class="hidden-md" src="https://wallex.ir/img/coins/dash.png" alt="DASH" width="20px" /> دش
                                            </a>
                                        </td>
                                        <td width="35%" class="price" id="WS-DASHUSDT" style="border: none;">70</td>
                                        <td width="25%" class="inc_change_pr" dir="ltr" style="border: none; color: #01dc8a;">0.25%</td>
                                    </tr>
                                    <tr class="coin-type hover_tether" data-key="USDT" id="tether" style="border: none;">
                                        <td width="40%" class="coin-name" style="border: none;">
                                            <a class="market_table_for_tablet" data-link="https://wallex.ir/app/trade/usdt-tmn" href="https://wallex.ir/app/trade/usdt-tmn">
                                                <img class="hidden-md" src="https://wallex.ir/img/coins/usdt.png" alt="Tether" width="20px" /> تتر
                                            </a>
                                        </td>
                                        <td width="35%" class="price" id="WS-USDTUSDT" style="border: none;">1.00</td>
                                        <td width="25%" class="inc_change_pr" dir="ltr" style="border: none; color: #01dc8a;">0.24%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </section>
                <section class="last_trades">
                    <div class="col-md-12">
                        <div class="last_trades_box">
                            <div class="row last_trade_box--title">
                                <span class="col-12 bz_t" style="color: black; opacity: 1;"> <strong> آخرین معاملات </strong> </span> <span class="bz_t trades-data-btc-tmn last_trades_title last_trades_market">بیت کوین - ریال </span>
                            </div>
                            <table class="table header-table">
                                <thead>
                                    <tr>
                                        <th width="40%">مقدار (BTC)</th>
                                        <th width="40%">قیمت واحد (تومان)</th>
                                        <th width="20%">زمان</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="on-tr table-responsive cscrl mCustomScrollbar _mCS_1 mCS-dir-ltr" style="padding: 0px; height: 0px;">
                                <div id="mCSB_1" class="mCustomScrollBox mCS-dark mCSB_vertical_horizontal mCSB_inside" style="max-height: none;" tabindex="0">
                                    <div id="mCSB_1_container_wrapper" class="mCSB_container_wrapper mCS_x_hidden mCS_no_scrollbar_x">
                                        <div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px; width: 100%;" dir="rtl">
                                            <table class="table table-hover trades_table" id="last_trades">
                                                <tbody>
                                                    <tr style="color: #cd2d57 !important;">
                                                        <td width="45%">0.01199531</td>
                                                        <td width="45%" class="tr-pr">198,511,112</td>
                                                        <td width="10%">۱۲:۵۵</td>
                                                    </tr>
                                                    <tr style="color: #cd2d57 !important;">
                                                        <td width="45%">0.02401546</td>
                                                        <td width="45%" class="tr-pr">198,520,000</td>
                                                        <td width="10%">۱۲:۵۵</td>
                                                    </tr>
                                                    <tr style="color: #cd2d57 !important;">
                                                        <td width="45%">0.060454</td>
                                                        <td width="45%" class="tr-pr">198,520,010</td>
                                                        <td width="10%">۱۲:۵۵</td>
                                                    </tr>
                                                    <tr style="color: #01dc8a !important;">
                                                        <td width="45%">0.0150263</td>
                                                        <td width="45%" class="tr-pr">199,650,000</td>
                                                        <td width="10%">۱۲:۵۳</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.00117893</td>
                                                        <td width="40%">198,520,000</td>
                                                        <td width="20%">۱۲:۵۱</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #01dc8a !important;">
                                                        <td width="40%">0.00100543</td>
                                                        <td width="40%">199,888,376</td>
                                                        <td width="20%">۱۲:۵۱</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.02518752</td>
                                                        <td width="40%">198,511,000</td>
                                                        <td width="20%">۱۲:۵۱</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.05398612</td>
                                                        <td width="40%">198,510,000</td>
                                                        <td width="20%">۱۲:۵۰</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.0143</td>
                                                        <td width="40%">198,511,000</td>
                                                        <td width="20%">۱۲:۵۰</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.29223428</td>
                                                        <td width="40%">198,500,000</td>
                                                        <td width="20%">۱۲:۴۸</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.068101</td>
                                                        <td width="40%">198,500,000</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.0749844</td>
                                                        <td width="40%">198,500,000</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.05</td>
                                                        <td width="40%">198,506,339</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.00542755</td>
                                                        <td width="40%">198,500,000</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.00293631</td>
                                                        <td width="40%">198,550,001</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.05751988</td>
                                                        <td width="40%">198,560,000</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #cd2d57 !important;">
                                                        <td width="40%">0.00199133</td>
                                                        <td width="40%">198,600,000</td>
                                                        <td width="20%">۱۲:۴۷</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #01dc8a !important;">
                                                        <td width="40%">0.01882002</td>
                                                        <td width="40%">199,999,999</td>
                                                        <td width="20%">۱۲:۴۵</td>
                                                    </tr>
                                                    <tr class="coin-type" style="color: #01dc8a !important;">
                                                        <td width="40%">0.17444186</td>
                                                        <td width="40%">199,900,000</td>
                                                        <td width="20%">۱۲:۴۵</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-dark mCSB_scrollTools_vertical" style="display: block;">
                                        <div class="mCSB_draggerContainer">
                                            <div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; display: block; height: 0px; top: 0px;">
                                                <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                                            </div>
                                            <div class="mCSB_draggerRail"></div>
                                        </div>
                                    </div>
                                    <div id="mCSB_1_scrollbar_horizontal" class="mCSB_scrollTools mCSB_1_scrollbar mCS-dark mCSB_scrollTools_horizontal" style="display: none;">
                                        <div class="mCSB_draggerContainer">
                                            <div id="mCSB_1_dragger_horizontal" class="mCSB_dragger" style="position: absolute; min-width: 30px; width: 0px; left: 0px;"><div class="mCSB_dragger_bar"></div></div>
                                            <div class="mCSB_draggerRail"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </section>
            </aside>
        </main> --}}

        {{-- nemoodar --}}
{{--         <main class="py-4">
            <div class="d-flex flex-row-reverse ">
                <div class="col-md-6 col-th ">
                    <div class="nav-tabs-custom trades_chart"> 
                        <ul class="nav nav-tabs"> 
                            <li class="active">
                                <a class="nav_tabs_border" href="#tab_global_trades" data-toggle="tab" aria-expanded="false">نمودار معاملات بازار جهانی
                                </a>
                            </li> 
                            <li>
                                <a class="nav_tabs_border" href="#tab_wallex_trades" data-toggle="tab" aria-expanded="true"> نمودار معاملات بازار والکس
                                </a>
                            </li> 
                        </ul> ‍ 
                        <div class="tab-content"> 
                            <div class="tab-pane active" id="tab_global_trades"> 
                                <div id="tv_global_chart_container" data-symbol="BINANCE:BTCUSDT">
                                    <div id="tradingview_c27be-wrapper" style="position: relative;box-sizing: content-box;width: 100%;height: 500px;margin: 0 auto !important;padding: 0 !important;font-family:Arial,sans-serif;">
                                        <div style="width: 100%;height: 500px;background: transparent;padding: 0 !important;">
                                            <iframe id="tradingview_c27be" src="https://s.tradingview.com/widgetembed/?frameElementId=tradingview_c27be&amp;symbol=BINANCE%3ABTCUSDT&amp;interval=D&amp;hidesidetoolbar=0&amp;symboledit=1&amp;saveimage=1&amp;toolbarbg=f1f3f6&amp;studies=%5B%5D&amp;theme=Light&amp;style=1&amp;timezone=Asia%2FTehran&amp;withdateranges=1&amp;studies_overrides=%7B%7D&amp;overrides=%7B%22paneProperties.topMargin%22%3A15%7D&amp;enabled_features=%5B%22header_fullscreen_button%22%5D&amp;disabled_features=%5B%22volume_force_overculay%22%5D&amp;locale=fa_IR&amp;utm_source=wallex.ir&amp;utm_medium=widget&amp;utm_campaign=chart&amp;utm_term=BINANCE%3ABTCUSDT" style="width: 100%; height: 100%; margin: 0 !important; padding: 0 !important;" frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen="" __idm_id__="934985729">
                                                
                                            </iframe>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="tab-pane" id="tab_wallex_trades"> 
                                <div id="tv_chart_container" data-symbol="BTCTMN">
                                    
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div>
            </div>
        </main> --}}

        <main class="py-4">
             @yield('chat')
        </main>
    </div>
{{--     <script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script> --}}

    <script src="{{ asset('js/main.js') }}" defer></script>
</body>
</html>
