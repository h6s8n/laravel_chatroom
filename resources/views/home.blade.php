@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @can("add user")
                    <a href="#">test can method "add user"</a>
                    @endcan

                </div>


            </div>
        </div>
    </div>
</div>


@endsection
@section('chat')
<div class="container">
    <div class="row ">
        <div class="col-md-8">
            <chat></chat>
        </div>
        <div class="col-md-4">
            <chat-users></chat-users>
        </div>
    </div>
</div>
@endsection 
